<?php


class db
{
    private $__connection;
    public $error;
    private $__errorMsg;

    public function __construct($initFile)
    {
        $config = parse_ini_file($initFile);
        try
        {
            $this->__connection = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbName'], $config['user'], $config['password']);
            $stmt = $this->__connection->prepare('SET NAMES utf8;');
            $stmt->execute();
            $this->error = false;
        }
        catch (PDOException $e)
        {
            $this->error = true;
            $this->__errorMsg = $e->getMessage();
        }
    }

    public function getErrorMsg()
    {
        if ($this->error) {
            return $this->__errorMsg;
        }
        else {
            return '';
        }
    }

    public function getRecords()
    {
        if ($this->error) {
            return array();
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('SELECT records.id, title, description, date, (SELECT COUNT(comments.id) FROM comments WHERE comments.recordId = records.id) AS comments FROM records;');
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return array();
            }
        }
    }

    public function getRecord($recordId)
    {
        if ($this->error) {
            return array();
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('SELECT * FROM records WHERE id = :id;');
                $stmt->bindParam(':id', $recordId);
                $stmt->execute();
                $record = $stmt->fetch(PDO::FETCH_ASSOC);
                if (empty($record)) {
                    $this->error = true;
                    $this->__errorMsg = 'Нет записи id '.$recordId.'!';
                }
                return $record;
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return array();
            }
        }
    }

    public function addRecord($title, $description)
    {
        if ($this->error) {
            return false;
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('INSERT INTO records (title, description) VALUES (:title, :description)');
                $stmt->bindParam(':title', $title);
                $stmt->bindParam(':description', $description);
                $stmt->execute();
                if ('00000' != $stmt->errorCode()) {
                    $this->error = true;
                    $this->__errorMsg = 'Ошибка SQL '.$stmt->errorCode().'!';
                    return false;
                }
                return true;
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return false;
            }
        }
    }

    public function getComments($recordId) {
        if ($this->error) {
            return array();
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('SELECT * FROM comments WHERE recordId = :recordId');
                $stmt->bindParam(':recordId', $recordId);
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return array();
            }
        }
    }

    public function addComment($name, $comment, $id)
    {
        if ($this->error) {
            return false;
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('INSERT INTO comments (recordId, name, comment) VALUES (:id, :name, :comment)');
                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':comment', $comment);
                $stmt->execute();
                if ('00000' != $stmt->errorCode()) {
                    $this->error = true;
                    $this->__errorMsg = 'Ошибка SQL '.$stmt->errorCode().'!';
                    return false;
                }
                return true;
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return false;
            }
        }
    }

    public function authUser($user, $pass) {
        if ($this->error) {
            return false;
        }
        else {
            try
            {
                $stmt = $this->__connection->prepare('SELECT pass FROM users WHERE name = :user;');
                $stmt->bindParam(':user', $user);
                $stmt->execute();
                $userData = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($userData['pass'] != md5($pass) || empty($userData)) {
                    $this->error = true;
                    $this->__errorMsg = 'Неверное имя пользователя или пароль!';
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (PDOException $e)
            {
                $this->error = true;
                $this->__errorMsg = $e->getMessage();
                return false;
            }
        }
    }
}