<?php
    include_once 'db.php';
    $db = new db('config/config.ini');
    $success = $db->addRecord($_POST['title'], $_POST['description']);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php
    if ($success) {
        echo 'Запись добавлена - вернитесь на <a href="index.php">главную</a>';
    }
    else {
        echo $db->getErrorMsg();
        echo '<br /> Ошибка! Вернитесь на <a href="index.php">главную</a>';
    }
?>
</body>
</html>