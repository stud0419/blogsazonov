<?php
    include_once 'db.php';
    session_start();
    $db = new db('config/config.ini');
    $id = $_REQUEST['id'];
    $bpost = $db->getRecord($id);
    $comments = $db->getComments($id);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <?php
            if ($db->error) {
                echo $db->getErrorMsg();
                echo '<br /> Ошибка! Вернитесь на <a href="index.php">главную</a>';
                exit;
            }
            else {
                echo '<div class="col-12">
                        <a href="index.php"><- На главную</a>
                        <div class="bpost">
                            <div class="ptitle">'.$bpost['title'].'</div>
                            <div class="post">'.$bpost['description'].'
                                <div class="date">'.$bpost['date'].'</div>
                            </div>
                        </div>
                      </div>
                ';
            }
        ?>
    </div>
    <div id="comments" class="comments row">
        <div class="ctitle col-12">Комментарии</div>
        <?php
            if (empty($comments)) {
                echo 'Нет комментариев';
            }
            else {
                foreach ($comments as $comment) {
                    echo '
                        <div class="comment-wrap col-10">
                            <div class="name">'.$comment['name'].'</div>
                            <div class="comment">'.$comment['comment'].'</div>
                        </div>
                    ';
                }
            }
        ?>
    </div>
    <div class="row">
        <div class="col-8">
            <form method="post" action="comment.php">
                <input type="hidden" id="id" name="id" value="<?=$id?>"/>
                <label> Имя</label>
                <input type="text" id="name" name="name" required />
                <br />
                <div>Оставить комментарий</div>
                <textarea rows="7" cols="70" id="comment" name="comment" required>

                </textarea>
                <br />
                <button class="btn btn-primary" id="submit" type="submit">Отправить</button>
                <div id="error" class="error"></div>
            </form>
        </div>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
        crossorigin="anonymous"></script>
<script type="text/javascript" src="js/ajax_comment.js"></script>

</body>
</html>
