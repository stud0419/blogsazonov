<?php
    session_start();
    if (!isset($_SESSION['auth'])) {
        header('Location: index.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
</head>
<body>
<form method="post" action="post.php">
    <label>Название</label>
    <input type="text" name="title" required />
    <br />
    <div>Описание</div>
    <textarea rows="100" cols="100" name="description" required>

    </textarea>
    <br />
    <button type="submit">Отправить</button>
</form>
</body>
</html>
