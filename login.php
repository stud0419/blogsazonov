<?php
    include_once 'db.php';
    session_start();
    $db = new db('config/config.ini');
    $name = $_POST['name'];
    $pass = $_POST['pass'];
    $success = $db->authUser($name, $pass);
    if ($success) {
        $_SESSION['auth'] = true;
        header('Location: index.php');
        exit;
    }
    ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php
    echo $db->getErrorMsg();
    echo '<br /> Ошибка! Вернитесь на <a href="index.php">главную</a>';
?>
</body>
</html>