$(document).ready(function () {
    $('#submit').click(function () {
        $.ajax({
                url: 'comment.php',
                data: {
                    id: $('#id').val(),
                    name: $('#name').val(),
                    comment: $('#comment').val()
                },
                dataType: 'json',
                type: 'post',
                success: function (response) {
                    if (response.success) {
                        $('#comments').append('<div class="comment-wrap"> ' +
                            '<div class="name">' + $('#name').val() + '</div> ' +
                            '<div class="comment">' + $('#comment').val() + '</div>' +
                            '</div>');
                        $('#error').css('display', 'none');
                    }
                    else {
                        $('#error').text(response.error).css('display', 'block');
                    }
                },
                error: function (e, message) {
                    $('#error').text(message).css('display', 'block');
                }
            });
        return false;
    });
});