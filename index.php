<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" />
</head>
<body>
<?php
    include_once 'db.php';
    session_start();
    $db = new db('config/config.ini');
    $records = $db->getRecords();
    if ($db->error) {
        echo $db->getErrorMsg();
    }
?>
    <div class="container">
        <div class="records row">
            <?php
                if (!empty($records)) {
                    foreach ($records as $record) {
                        echo '<div class="record col-12">
                                <a href="bpost.php?id='.$record['id'].'"><div class="title">'.$record['title'].'</div></a>
                                <div class="desc">'.$record['description'].'</div>
                                <div class="date">Добалено: '.$record['date'].'</div>
                                <div class="count">Комментарии: '.$record['comments'].'</div>
                              </div>';
                    }
                }
                else {
                    echo 'Нет записей';
                }
            ?>
        </div>
        <div class="row">
        <?php
        if (isset($_SESSION['auth'])) {
            echo '<div class="add col-6">
                    <a class="btn btn-primary" href="add.php">Добавить</a>
                    <a class="btn btn-primary" href="logout.php">Выйти</a>
                    </div>';
        }
        else {
            echo '<div class="auth col-3">
                    <a class="btn btn-primary" href="auth.php">Логин</a>
                    </div>';
        }
        ?>
    </div>
    </div>
</body>
</html>
