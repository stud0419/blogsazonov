<?php
    include_once 'db.php';
    $db = new db('config/config.ini');
    $pattern = '/^[А-Яа-я\ \n\r]*$/u';
    $id = $_POST['id'];
    $name = $_POST['name'];
    $comment = $_POST['comment'];

    if (!preg_match($pattern, $comment)) {
        echo json_encode(['success' => false, 'error' => 'В комментариях разрешены только буквы русского алфавита и пробелы!']);
    }
    elseif (empty($name) || empty($comment)) {
        echo json_encode(['success' => false, 'error' => 'Имя и комментарий - обязательне поля!']);
    }
    else {
        $success = $db->addComment($name, $comment,  $id);
        if ($success) {
            echo json_encode(['success' => true, 'error' => 'none']);
        }
        else {
            echo json_encode(['success' => false, 'error' => $db->getErrorMsg()]);
        }
    }
